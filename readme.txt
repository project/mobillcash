Payment Module  : MOBILLCASH
Original Author : Dublin Drupaller
Settings        : > administer > store > settings > mobillcash

********************************************************************
DESCRIPTION:

Accept mobile payments using the mCommerce payment gateway Mobillcash.
Setting up an mCommerce gateway account with Mobillcash is free. Click on the
link http://www.mobillcash.com/merchant/?aff_id=AF087151 to sign up.

Contact Dublin Drupaller via http://www.DublinDrupaller.com for assistance
or for suggestions, ideas or improvements.
********************************************************************


INSTALLATION
------------------
Pre-installation note: After downloading the module from Drupal.org,
it is recommended you upload the Mobillcash module files to /sites/all/modules/ecommerce/contrib/mobillcash

(a) Enabled the module as you would any other Drupal module under ADMINISTER -> SITE BUILDING -> MODULES

(b) Once enabled go to the Mobillcash module settings page.

ADMINISTER > E-COMMERCE CONFIGURATION -> MOBILLCASH

(c) Enter your Mobillcash Merchant ID and  Mobillcash Notification Authentication password. Both are
found on the GENERAL ACCOUNT SETTINGS page from your mobillcash account control panel.

(d) Enter you Service name
This is a short text string, no longer than 20 GSM characters, that describes the service for which payment is being taken.
This value appears in all text messages sent to the user.

(e) Enter your secret billing status notification URL.
This is the URL the Mobillcash server uses for Instant Payment Notification and it's important that you
create a very random filename and make sure it matches your Notification URL in your
Mobillcash Advanced Billing Server Settings.

(f) Enter the latest valid IP addresses from Mobillcash (seperated by a comma and space).
This makes sure only requests from the Mobillcash server are being sent when a payment notification
is being sent to your secret secret billing status notification URL.
Click on the link below for the latest list of valid IP addresses:
http://gateway.vidicom.co.uk/server_IPs.txt

(g) Specify which currency you want to charge for your services/products.
Mobillcash will convert to users local currency automatically.

(h) Specifiy Mobillcash Charge Type
Charge mode: Customer pays the amount specified by the merchant with no additional costs.
Revenue mode: Consumer pays all network fees. Mobillcash charges a transaction processing fee to the merchant.
Full pay mode: Customer pays price, network fees and the transaction processing fee

(I) Specify the maximum purchase amount (gross) that will be accepted for Mobillcash payments.


UNINSTALL
-------------

Uninstall routine is included, ADMINISTER -> SITE BUILDING -> MODULES


NOTES
---------

More detailed documentation will be compiled and submitted to the Drupal handbook soon.
For support/assistance, or if you have any ideas for improvements, please
contact Dublin Drupaller: dub@dublindrupaller.com

